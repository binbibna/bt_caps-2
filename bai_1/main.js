const URL = "https://63d767b9afbba6b7c93c6833.mockapi.io";

function renderSPList(sp) {
  var contentHTML = "";

  sp.reverse().forEach(function (item) {
    var contentTr = `
      <div class="card">
          <div class="top-bar">
              <i class="fab fa-apple"></i>
                  <em class="stocks">${item.type}</em>
          </div>
          <div class="img-container">
              <img class="product-img" src="${item.img}" alt="">
              <div class="out-of-stock-cover"><span>Out Of Stock</span></div>
          </div>
          <div class="details">
              <div class="name-fav">
                  <strong class="product-name">${item.name}</strong>
                      <button onclick="this.classList.toggle(&quot;fav&quot;)" class="heart"><i class="fas fa-heart"></i></button>
              </div>
              <div class="wrapper">
                  <h5>${item.desc}</h5>
                  <p>Camera sau : ${item.backCamera}</p>
                  <p>Camera trước : ${item.frontCamera}</p>
              </div>
              <div class="purchase">
                  <p class="product-price">$ ${item.price}</p>
                  <span class="btn-add">
                      <div>
                           <button id="${item.id}" onclick="addCart(${item.id})" class="add-btn">Add <i class="fas fa-chevron-right"></i></button>
                       </div>
                  </span>
              </div>
          </div>
      </div>
    `;
    contentHTML += contentTr;
  });
  document.getElementById("item").innerHTML = contentHTML;
}

// gọi api lấy danh sách món ăn từ server
function fetchSpList() {
  // bật loading 1 lần trước khi api được gọi
  // tắt loading 2 lần trong then và catch
  // batLoading();
  axios({
    url: `${URL}/Product`,
    method: "GET",
  })
    .then(function (res) {
      // tatLoading();
      var SPList = res.data;
      renderSPList(SPList);
    })
    .catch(function (err) {
      // tatLoading();
      console.log(`  🚀: err`, err);
    });
}
// khi user load trang
fetchSpList();

function timkiem(obj) {
  var ten = document.getElementById("txt-tim").value;
  function fetchSpList() {
    axios({
      url: `${URL}/Product`,
      method: "GET",
    })
      .then(function (res) {
        // tatLoading();
        var arr = [];
        var SPList = res.data;
        for (var index = 0; index < SPList.length; index++) {
          if (ten === SPList[index].type) {
            arr.push(SPList[index]);
          }
        }
        console.log(arr);
        var contentHTML = "";

        arr.reverse().forEach(function (item) {
          var contentTr = `
      <div class="card">
          <div class="top-bar">
              <i class="fab fa-apple"></i>
                  <em class="stocks">${item.type}</em>
          </div>
          <div class="img-container">
              <img class="product-img" src="${item.img}" alt="">
              <div class="out-of-stock-cover"><span>Out Of Stock</span></div>
          </div>
          <div class="details">
              <div class="name-fav">
                  <strong class="product-name">${item.name}</strong>
                      <button onclick="this.classList.toggle(&quot;fav&quot;)" class="heart"><i class="fas fa-heart"></i></button>
              </div>
              <div class="wrapper">
                  <h5>${item.desc}</h5>
                  <p>Camera sau : ${item.backCamera}</p>
                  <p>Camera trước : ${item.frontCamera}</p>
              </div>
              <div class="purchase">
                  <p class="product-price">$ ${item.price}</p>
                  <span class="btn-add">
                      <div>
                           <button  class="add-btn" onclick="addCart(${item.id})">Add <i class="fas fa-chevron-right"></i></button>
                       </div>
                  </span>
              </div>
          </div>
      </div>
    `;
          contentHTML += contentTr;
        });
        document.getElementById("item").innerHTML = contentHTML;
      })
      .catch(function (err) {
        // tatLoading();
        console.log(`  🚀: err`, err);
      });
  }
  // khi user load trang
  fetchSpList();
}
const cartItem = document.querySelector(".cart-items"); // Item trong giỏ hàng
function renderCart() {
  var cartList = JSON.parse(localStorage.getItem("cartList")) || []; // lấy danh sách sản phẩm trong giỏ hàng
  cartItem.innerHTML = "";
  cartList.forEach(function (item) {
    // duyệt danh sách sản phẩm trong giỏ hàng
    var contentHTML = "";
    var contentTr = `
    <div class="cart-item" data-id="${item.id}">
    <img class="cart-item-image" src="${item.img}" alt="">
    <div class="cart-item-info">
        <h4 class="cart-item-name">${item.name}</h4>
        <p class="cart-item-price">$${item.price}</p>
        <button class="btn btn-danger" onclick="changeQuantity(${item.id}, -1)">-</button>
        <input class="cart-quantity-input"  type="number" value="${item.quantity}">
        <button class="btn btn-danger" onclick="changeQuantity(${item.id}, 1)">+</button>
        <button class="btn btn-danger" onclick="removeItem(${cartList.index})">Xóa</button>
    </div>
</div>
  `;
    contentHTML += contentTr;
    cartItem.innerHTML += contentHTML;

    // tính tổng tiền của giỏ hàng và hiển thị lên giao diện
    var total = 0;
    cartList.forEach(function (item) {
      // duyệt danh sách sản phẩm trong giỏ hàng
      total += item.price * item.quantity;
    });
    document.querySelector(".total").innerHTML = total; // hiển thị tổng tiền lên giao diện
  });
}
renderCart();
function addCart(id) {
  // thêm sản phẩm vào giỏ hàng
  axios({
    url: `${URL}/Product/${id}`,
    method: "GET",
  })
    .then(function (res) {
      var SPList = res.data; // lấy thông tin sản phẩm
      var cartList = JSON.parse(localStorage.getItem("cartList")) || []; // lấy danh sách sản phẩm trong giỏ hàng
      var index = cartList.findIndex(function (item) {
        // tìm vị trí sản phẩm trong giỏ hàng
        return item.id === SPList.id; // nếu tìm thấy trả về vị trí, không tìm thấy trả về -1
      });
      if (index === -1) {
        // nếu không tìm thấy sản phẩm trong giỏ hàng
        SPList.quantity = 1; // thêm thuộc tính quantity vào sản phẩm
        cartList.push(SPList); // thêm sản phẩm vào giỏ hàng
      } else {
        // nếu tìm thấy sản phẩm trong giỏ hàng
        cartList[index].quantity += 1; // tăng số lượng sản phẩm trong giỏ hàng
      }
      localStorage.setItem("cartList", JSON.stringify(cartList)); // lưu danh sách sản phẩm vào localStorage
      cartItem.innerHTML = "";
      cartList.forEach(function (item) {
        // duyệt danh sách sản phẩm trong giỏ hàng
        var contentHTML = ""; // nội dung html của sản phẩm
        var contentTr = `
        <div class="cart-item" data-id="${item.id}">
        <img class="cart-item-image" src="${item.img}" alt="">
        <div class="cart-item-info">
            <h4 class="cart-item-name">${item.name}</h4>
            <p class="cart-item-price">$${item.price}</p>
            <button class="btn btn-danger" onclick="changeQuantity(${item.id},-1)">-</button>
            <input class="cart-quantity-input"  type="number" value="${item.quantity}">
            <button class="btn btn-danger" onclick="changeQuantity(${item.id},1)">+</button>
            <button class="btn btn-danger" onclick="removeItem(${item.id})">Xóa</button>
        </div>
    </div>
      `;
        contentHTML += contentTr;
        cartItem.innerHTML += contentHTML; // hiển thị sản phẩm lên giao diện
      });
    })
    .catch(function (err) {
      // tatLoading();
      console.log(`  🚀: err`, err);
    });
}
// khi click vào button + hoặc - thì thay đổi số lượng của sản phẩm đó trong giỏ hàng
function changeQuantity(id, number) {
  var cartList = JSON.parse(localStorage.getItem("cartList")) || []; // lấy danh sách sản phẩm trong giỏ hàng
  cartList.forEach(function (item) {
    // duyệt danh sách sản phẩm trong giỏ hàng
    if (Number(item.id) === id) {
      // nếu id của sản phẩm trong giỏ hàng trùng với id của sản phẩm đang thay đổi số lượng
      item.quantity += number; // thay đổi số lượng sản phẩm
    }
  });
  localStorage.setItem("cartList", JSON.stringify(cartList)); // lưu danh sách sản phẩm vào localStorage
  renderCart(); // hiển thị lại giỏ hàng
}

// xóa sản phẩm khỏi giỏ hàng theo id của sản phẩm đó
function removeItem(id) {
  var cartList = JSON.parse(localStorage.getItem("cartList")) || []; // lấy danh sách sản phẩm trong giỏ hàng
  cartList.splice(id, 1); // xóa sản phẩm khỏi giỏ hàng
  localStorage.setItem("cartList", JSON.stringify(cartList)); // lưu danh sách sản phẩm vào localStorage
  renderCart(); // hiển thị lại giỏ hàng
}
function clearCart() {
  localStorage.removeItem("cartList"); // xóa danh sách sản phẩm trong giỏ hàng
  renderCart(); // hiển thị lại giỏ hàng
}

function buy() {
  alert("Mua hàng thành công"); // hiển thị thông báo mua hàng thành công
  clearCart(); // xóa giỏ hàng
}
const numberProducts = document.querySelector(".text"); // số lượng sản phẩm trong giỏ hàng
function numberCart() {
  var cartList = JSON.parse(localStorage.getItem("cartList")) || []; // lấy danh sách sản phẩm trong giỏ hàng
  numberProducts.innerText = cartList.length; // hiển thị số lượng sản phẩm trong giỏ hàng
}
numberCart(); // hiển thị số lượng sản phẩm trong giỏ hàng
