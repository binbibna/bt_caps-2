const URL ="https://63d767b9afbba6b7c93c6833.mockapi.io";

document.getElementById("MaSP").disabled=true;

function renderSpList(sp) {

    var contentHTML = "";
  
    sp.reverse().forEach(function (item) {
      var contentTr = ` <tr>
                          <td>${item.masp}</td>
                          <td>${item.name}</td>
                          <td>${item.price}</td>
                          <td>
                          ${convertString(50, item.img)}
                          </td>
                          <td>
                          ${item.mota}
                          </td>
                          <td>
                          <button onclick="xoaSP('${
                            item.masp
                          }')" class="btn btn-danger">Xoá</button>
                          <button onclick="suaSP('${
                            item.masp
                          }')" class="btn btn-warning">Sửa</button>
                          </td>
                        </tr>`;
      contentHTML += contentTr;
    });
    document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
    console.log(sp);
  }



function fetchSpList() {
    // bật loading 1 lần trước khi api được gọi
    // tắt loading 2 lần trong then và catch
    // batLoading();
    axios({
      url: `${URL}/Products`,
      method: "GET",
    })
      .then(function (res) {
        // tatLoading();
        var sp = res.data;
        renderSpList(sp);
      })
      .catch(function (err) {
        // tatLoading();
        console.log(`  🚀: err`, err);
      });
  }
  // khi user load trang
  fetchSpList();


  // thêm món ăn

function themSP() {
  var sp = layThongTinTuForm();
  var isValid = true;
   
  isValid = kiemTraDoDai(sp.name, "tbname",4,200) & kiemTraSo(sp.price, "tbprice");
  isValid = isValid & kiemTraDoDai(sp.price, "tbprice" , 6,20) & kiemTraDoDai(sp.img, "tbimg", 1,1000);
  isValid = isValid & kiemTraDoDai(sp.mota, "tbmota",1,200);
  if(isValid){
  // batLoading();
  axios({
    url: `${URL}/Products`,
    method: "POST",
    data: sp,
  })
    .then(function (res) {
      // tatLoading();
      fetchSpList();
    })
    .catch(function (err) {
      // tatLoading();
    });
  document.getElementById("myModal").style.display="none";
  const element = document.querySelector(".modal-backdrop");
  element.remove();
  }
}
// pending, resolve ( success ), reject ( fail )

function suaSP(id) {
  axios({
    url: `${URL}/Products/${id}`,
    method: "GET",
  })
    .then(function (res) {
      var sp = res.data;
      showThongTin(sp);
      document.getElementById("MaSP").disabled=true;
      console.log(sp.masp);
    })
    .catch(function (err) {
    });
    $("#myModal").modal("show");
  }



  function xoaSP(id) {
   
    // batLoading();
    axios({
      url: `${URL}/Products/${id}`,
      method: "DELETE",
    })
      .then(function (res) {
        // tatLoading();
        // gọi lại api lấy danh sách  sau khi xoá thành công
        fetchSpList();
      })
      .catch(function (err) {
        // tatLoading();
      });
  }




function capnhatSP(id) {
  var sp = layThongTinTuForm();
  // batLoading();
  axios({
    url: `${URL}/Products/${id}`,
    method: "PUT",
    data: sp,
  })
    .then(function (res) {
      // tatLoading();
      fetchSpList();
    })
    .catch(function (err) {
      // tatLoading();
    });
  document.getElementById("myModal").style.display="none";
  const element = document.querySelector(".modal-backdrop");
  element.remove();
}