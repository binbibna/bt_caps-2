
function kiemTraDoDai(value,idErr,min,max){
    var length = value.length;
    if ( length < min || length > max){
        document.getElementById(idErr).innerText=`Độ dài ký tự phải từ ${min}-${max}`;
        document.getElementById(idErr).style.display="block"
        return false;
    }else{
        document.getElementById(idErr).innerText=``;
        return true;       
    }
}

function kiemTraSo(value, idErr){
    var reg = /^\d+$/;
    var isNumber = reg.test(value);
    if(isNumber){      
        document.getElementById(idErr).innerText=`vui lòng nhập số`;
        document.getElementById(idErr).style.display="block"
        return false;
    }else{
        document.getElementById(idErr).innerText=``;
        return true;
    }
}



